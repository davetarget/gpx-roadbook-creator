<?php

namespace LGnap\Reader;

use DOMDocument;
use DOMNode;
use DOMXPath;
use LGnap\Model\GpxWaypoint;

class GpxReader
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return GpxWaypoint[]
     */
    public function extractWaypoints(): array
    {
        $gpxWaypoints = [];

        $domDoc = new DOMDocument();
        $domDoc->load($this->filename);

        $domXPath = new DOMXPath($domDoc);

        $domXPath->registerNamespace('g', 'http://www.topografix.com/GPX/1/0');
        $domXPath->registerNamespace('gs', 'http://www.groundspeak.com/cache/1/0/1');
        $domXPath->registerNamespace('gsak', 'http://www.gsak.net/xmlv1/6');

        $waypoints = $domXPath->query('//g:wpt');

        if (! $waypoints) {
            error_log('error');
            return [];
        }

        $waypointReader = new WaypointReader($domXPath);

        foreach ($waypoints as $waypoint) {
            $gpxWaypoints[] = $waypointReader->extractWayPoint($waypoint);
        }

        return $gpxWaypoints;
    }
}
