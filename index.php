<?php

use LGnap\Reader\GpxReader;

require_once 'vendor/autoload.php';

$gpxReader = new GpxReader();

$gpxWaypoints = $gpxReader->extractWaypoints();

// Creating the new document...
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('source-complete.docx');

$templateProcessor->setValues([
    'title' => 'Nounourse',
    'parking' => 'Rue du lit 24, 1400 Maison'
]);

$values = [];

foreach ($gpxWaypoints as $gpxWaypoint) {
    $values[] = $gpxWaypoint->toArray();
}

$templateProcessor->cloneRowAndSetValues('gccode', $values);

$templateProcessor->saveAs('Test.docx');
