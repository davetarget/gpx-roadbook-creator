<?php

namespace LGnap\Reader;

use DOMNode;
use DOMXPath;
use LGnap\Model\GpxWaypoint;

class WaypointReader
{
    private DOMXPath $DOMXPath;

    public function __construct(DOMXPath $DOMXPath)
    {
        $this->DOMXPath = $DOMXPath;
    }

    public function extractWayPoint(DOMNode $waypoint): GpxWaypoint
    {
        $gpxWaypoint = new GpxWaypoint();
        $gcCodeNode = $this->DOMXPath->query('./g:name', $waypoint);

        if (! $gcCodeNode || $gcCodeNode->length !== 1) {
            continue;
        }
        $gpxWaypoint->setGcCode($gcCodeNode->item(0)->nodeValue);

        $gcCodeNode = $this->DOMXPath->query('./gsak:wptExtension/gsak:GcNote', $waypoint);
        if ($gcCodeNode && $gcCodeNode->length === 1) {
            foreach ($gcCodeNode->item(0)->childNodes as $cn) {
                echo $cn->nodeValue . "\n";
            }
        }

        $groundspeakCache = $this->DOMXPath->query('./gs:cache', $waypoint);
        if (! $groundspeakCache || $groundspeakCache->length !== 1) {
            continue;
        }
        foreach ($groundspeakCache->item(0)->childNodes as $waypointChildNode) {

            switch ($waypointChildNode->nodeName) {
                default: //silent uninteresting nodes
                    break;
                case 'groundspeak:name':
                    $gpxWaypoint->setName($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:type':
                    $gpxWaypoint->setType($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:difficulty':
                    $gpxWaypoint->setDifficulty($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:terrain':
                    $gpxWaypoint->setTerrain($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:encoded_hints':
                    $gpxWaypoint->setHint($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:long_description':
                    $gpxWaypoint->setDescription($waypointChildNode->nodeValue);
                    break;
            }
        }
        return $gpxWaypoint;
    }
}
