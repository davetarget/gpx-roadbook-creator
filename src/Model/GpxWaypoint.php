<?php

namespace LGnap\Model;

class GpxWaypoint
{
    private string $gcCode;
    private string $name;
    private string $type;
    private int $difficulty;
    private int $terrain;
    private string $hint;
    private string $description;

    /**
     * @return string
     */
    public function getGcCode(): string
    {
        return $this->gcCode;
    }

    /**
     * @param string $gcCode
     */
    public function setGcCode(string $gcCode): void
    {
        $this->gcCode = $gcCode;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    /**
     * @param int $difficulty
     */
    public function setDifficulty(int $difficulty): void
    {
        $this->difficulty = $difficulty;
    }

    /**
     * @return int
     */
    public function getTerrain(): int
    {
        return $this->terrain;
    }

    /**
     * @param int $terrain
     */
    public function setTerrain(int $terrain): void
    {
        $this->terrain = $terrain;
    }

    /**
     * @return string
     */
    public function getHint(): string
    {
        return $this->hint;
    }

    /**
     * @param string $hint
     */
    public function setHint(string $hint): void
    {
        $this->hint = $hint;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function toArray(): array
    {
        return [
            'gccode' => $this->getGcCode(),
            'cachename' => $this->getName(),
            'difficulty' => $this->getDifficulty(),
            'terrain' => $this->getTerrain(),
            'hint' => $this->getHint()
        ];
    }
}
